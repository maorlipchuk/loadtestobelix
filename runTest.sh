set -x
REPO="docker056588154"
REPO_FOLDER="loadtestobelix"
IMAGE_NAME="loadtest"
JOB_NAME="${IMAGE_NAME}-job"
K6_REQUEST_FILE="req.js"
JOB_FILE_NAME="${IMAGE_NAME}.yaml"
SVC_SUFFIX_NAME="obelix"
NAMESPACE="ves-system"
PORT="18096"
CURRENT_PATH="`dirname \"$0\"`"

# end point gettoken/insight
END_POINT="ingress\/gettoken"
#END_POINT="ingress\/insight"

# source payload
#SRC_PAYLOAD=".\/src\/payload_updated.json"
#SRC_PAYLOAD=".\/src\/innocent_gettoken.json"
SRC_PAYLOAD=".\/src\/f5GetToken.json"
#SRC_PAYLOAD=".\/src\/innocent_gettoken_origin.json"

# k6 features
VUS="10"
DURATION="30s"

export HTTP_API=`kubectl get svc ${SVC_SUFFIX_NAME} -n ${NAMESPACE} -ojsonpath='{.spec.clusterIP}'`
export CLUSTER_IP=`kubectl get svc ${SVC_SUFFIX_NAME} -n ${NAMESPACE} -ojsonpath='{.spec.clusterIP}'`

echo "Cluster IP: " ${CLUSTER_IP}
cat $CURRENT_PATH/templates/$JOB_FILE_NAME.template | sed "s/{{VUS}}/$VUS/g;\
                                      s/{{DURATION}}/$DURATION/g;\
                                      s/{{JOB_NAME}}/$JOB_NAME/g;\
                                      s/{{REQUEST_FILE}}/$K6_REQUEST_FILE/g;\
                                      s/{{IMAGE}}/$REPO\/$REPO_FOLDER/g;" > $CURRENT_PATH/$JOB_FILE_NAME

cat $CURRENT_PATH/templates/$K6_REQUEST_FILE.template | sed "s/{{IP}}/$CLUSTER_IP/g;\
                                      s/{{NAMESPACE}}/$NAMESPACE/g;\
                                      s/{{PAYLOAD}}/$SRC_PAYLOAD/g;\
                                      s/{{ENDPOINT}}/$END_POINT/g;\
                                      s/{{PORT}}/$PORT/g;" > $CURRENT_PATH/$K6_REQUEST_FILE
docker build -t $REPO/$REPO_FOLDER $CURRENT_PATH
docker push $REPO/$REPO_FOLDER
kubectl delete job $JOB_NAME -n ${NAMESPACE}
kubectl apply -f $CURRENT_PATH/$JOB_FILE_NAME -n ${NAMESPACE}
