import http from 'k6/http';

export let options = {
  insecureSkipTLSVerify: true,
  tlsAuth: [
    {
      domains: ['10.3.0.16'],
      cert: open('./identity/client.crt'),
      key: open('./identity/client.key'),
    },
  ],
  };
  var payload = open('./src/f5GetToken.json');

  export default function () {
     var url = 'https://10.3.0.16:18096/ingress/gettoken';
     var res = http.post(url, payload);
  }
