#!/bin/sh
go tool pprof -png heap > heap.png
go tool pprof -png cpu > cpu.png
go tool pprof -png goroutine > goroutine.png
